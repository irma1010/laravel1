<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SanberBook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form id="signup" action="/welcome" method="POST">
        @csrf
        <label>First name:</label>
        <br>
        <br>
        <input type="text" id="fname" name="fname">
        <br>
        <br>
        <label>Last name:</label>
        <br>
        <br>
        <input type="text" id="lname" name="lname">
        <br>
        <br>
        <label>Gender:</label>
        <br>
        <br>
        <input type="radio" id="Male" name="gender" value="Male">Male
        <br>
        <input type="radio" id="Female" name="gender" value="Female">Female
        <br>
        <input type="radio" id="Other" name="gender" value="Other">Other 
        <br>
        <br>
        <label>Nationality:</label>
        <br>
        <br>
        <select id="nat" name ="nat">
            <option value="indonesia">Indonesia</option>
            <option value="inggris">Inggris</option>
            <option value="japan">Japan</option>
            <option value="korea">Korea</option>
            <option value="german">German</option>
        </select>
        <br>
        <br>
        <label>Language Spoken:</label>
        <br>
        <br>
        <input type="checkbox" id="indonesia" name="lang" value="indonesia"> Indonesia
        <br>
        <input type="checkbox" id="english" name="lang" value="english"> English
        <br>
        <input type="checkbox" id="other" name="lang" value="other"> Other
        <br>
        <br>
        <label>Bio:</label>
        <br>
        <br>
        <textarea rows="8" cols="30" name="bio"></textarea>
        <br>
        <br>
        <button type="submit" form="signup" value="submit">Sign Up</button>

    </form>
</body>
</html>



